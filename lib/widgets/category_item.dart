import 'package:flutter/material.dart';
import '../screens/category_meals_screen.dart';

class CategoryItem extends StatelessWidget {
  final String id;
  final String title;
  final Color color; // Background color

  CategoryItem(this.id, this.title, this.color);

  void selectCategory(BuildContext ctx) {
    // Navigator has to be connected to the context, because the context
    // has information for this widget and its position in the tree
    // builder defines which widget should be build on the current widget
    // pass context to the builder function named as _
    Navigator.of(ctx).pushNamed(
      CategoryMealsScreen.routeName,
      arguments: {
        'id': id,
        'title': title,
      },
    );
  }

  // Creates a general look of a category item
  @override
  Widget build(BuildContext context) {
    // InkWell is a gesture detector with ripple effect
    return InkWell(
      onTap: () => selectCategory(context),
      splashColor: Theme.of(context).primaryColor,
      // This should match the border radius for the container bellow
      borderRadius: BorderRadius.circular(15),
      child: Container(
          padding: EdgeInsets.all(15),
          child: Text(
            title,
            style: Theme.of(context).textTheme.headline6,
          ),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                color.withOpacity(0.7),
                color,
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
            // Border radius of the card of the container
            borderRadius: BorderRadius.circular(15),
          )),
    );
  }
}
