import 'package:flutter/material.dart';

import '../widgets/meal_item.dart';
import '../models/meal.dart';

// Shows meals for a category

class CategoryMealsScreen extends StatefulWidget {
  static const routeName = '/category-meals';

  final List<Meal> availableMeals;

  CategoryMealsScreen(this.availableMeals);

  @override
  _CategoryMealsScreenState createState() => _CategoryMealsScreenState();
}

class _CategoryMealsScreenState extends State<CategoryMealsScreen> {
  String categoryTitle;
  List<Meal> displayedMeals;
  var _loadedInitData = false;

  // The method loads the data when the _CategoryMealsScreenState is created
  @override
  void initState() {

    super.initState();
  }

  @override
  void didChangeDependencies() {
    // Extract the route arguments map from the category item pushNamed
    if(!_loadedInitData){
      // If we have not loaded the initial data
      final routeArgs =
      ModalRoute.of(context).settings.arguments as Map<String, String>;
      categoryTitle = routeArgs['title'];
      final categoryId = routeArgs['id'];
      displayedMeals = widget.availableMeals.where((meal) {
        return meal.categories.contains(categoryId);
      }).toList();
      // Mark that the initial data is loaded
      _loadedInitData = true;
    }

    super.didChangeDependencies();
  }

  // When we open a meal and click the delete icon,
  // then it is removed from the list of meals also

  // Update the meals and remove a meal with that id from the list
  void _removeMeal(String mealId){
    setState(() {
      displayedMeals.removeWhere((meal) => meal.id == mealId);
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(categoryTitle),
      ),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return MealItem(
            id: displayedMeals[index].id,
            title: displayedMeals[index].title,
            imageUrl: displayedMeals[index].imageUrl,
            duration: displayedMeals[index].duration,
            affordability: displayedMeals[index].affordability,
            complexity: displayedMeals[index].complexity,
          );
        },
        // The amount of items we have
        itemCount: displayedMeals.length,
      ),
    );
  }
}
