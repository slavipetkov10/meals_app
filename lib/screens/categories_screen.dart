import 'package:flutter/material.dart';
import '../dummy_data.dart';
import '../widgets/category_item.dart';

// The class is loaded as a starting screen of the application
class CategoriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  GridView(
        padding: EdgeInsets.all(25),
        // Return a list of category item widget
        children: DUMMY_CATEGORIES
            .map((catData) => CategoryItem(
                  catData.id,
                  catData.title,
                  catData.color,
                ))
            .toList(),
        // Sliver is a scrollable area on the screen, this class allows to define
        // a max grid for each item and automatically creates the number of columns
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          // Set the width of the item
          maxCrossAxisExtent: 200,
          // How items should be sized regarding height and width relation
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 20,
          mainAxisSpacing: 20,
        ),
    );
  }
}
