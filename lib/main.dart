import 'package:flutter/material.dart';

import './dummy_data.dart';
import './screens/filters_screen.dart';
import './screens/tabs_screen.dart';
import './screens/meal_detail_screen.dart';
import './screens/category_meals_screen.dart';
import './screens/categories_screen.dart';
import './models/meal.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filters = {
    'gluten': false,
    'lactose': false,
    'vegan': false,
    'vegetarian': false,
  };

  List<Meal> _availableMeals = DUMMY_MEALS;

  // The list is passed to TabsScreen and from there it is passed to
  // FavoritesScreen
  List<Meal> _favoriteMeals = [];

  // Should be called from filters screen when a user clicks a button
  // in the app bar
  void _setFilters(Map<String, bool> filterData) {
    setState(() {
      // The use selected filters are passed to this method and set in
      // _filters and based on them in the _availableMeals are returned the
      // meals which correspond to that filters, then _availableMeals
      // are passed to the CategoryMealsScreen and then are displayed the meals
      _filters = filterData;

      _availableMeals = DUMMY_MEALS.where((meal) {
        if (_filters['gluten'] && !meal.isGlutenFree) {
          // If _filters['gluten'] is true, exclude fields that have gluten in it
          // If it is filtering for isGlutenFree
          return false;
        }
        if (_filters['lactose'] && !meal.isLactoseFree) {
          return false;
        }
        if (_filters['vegan'] && !meal.isVegan) {
          return false;
        }
        if (_filters['vegetarian'] && !meal.isVegetarian) {
          return false;
        }
        // Either all filters are satisfied or no filters are set and the
        // current meal is kept in the _availableMeals
        return true;
      }).toList();
    });
  }

  // The method is passed to MealDetailsScreen, then use the method on
  // FloatingActionButton
  void _toggleFavorite(String mealId) {
    // if the meal is part of the list it will be removed, else it will be
    // added to the list
    final existingIndex =
        _favoriteMeals.indexWhere((meal) => meal.id == mealId);
    if (existingIndex >= 0) {
      setState(() {
        _favoriteMeals.removeAt(existingIndex);
      });
    } else {
      // If the meal was not found in the list
      setState(() {
        _favoriteMeals.add(
          DUMMY_MEALS.firstWhere((meal) => meal.id == mealId),
        );
      });
    }
  }

  // Check if a meal is part of the favorite list
  bool _isMealFavorite(String id) {
    return _favoriteMeals.any((meal) => meal.id == id);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'DeliMeals',
        theme: ThemeData(
          primarySwatch: Colors.pink,
          accentColor: Colors.amber,
          // The color behind the items
          canvasColor: Color.fromRGBO(255, 254, 229, 1),
          // The main font
          fontFamily: 'Raleway',
          textTheme: ThemeData.light().textTheme.copyWith(
                bodyText2: TextStyle(
                  color: Color.fromRGBO(20, 51, 51, 1),
                ),
                bodyText1: TextStyle(
                  color: Color.fromRGBO(20, 51, 51, 1),
                ),
                headline6: TextStyle(
                  fontSize: 20,
                  fontFamily: 'RobotoCondensed',
                  fontWeight: FontWeight.bold,
                ),
              ),
        ),
        // home: CategoriesScreen(),
        // The String key is a route, which is a screen, the value is the creation
        // function for the screen
        initialRoute: '/',
        routes: {
          '/': (ctx) => TabsScreen(_favoriteMeals),
          // /category-meals
          CategoryMealsScreen.routeName: (ctx) =>
              CategoryMealsScreen(_availableMeals),
          MealDetailScreen.routeName: (ctx) =>
              MealDetailScreen(_toggleFavorite, _isMealFavorite),
          FiltersScreen.routeName: (ctx) =>
              FiltersScreen(_filters, _setFilters),
        },
        // Can be used to show which settings were pushed, or if routes are
        // generated dynamically
        onGenerateRoute: (settings) {
          print(settings.arguments);
          // if(settings.name == '/meal-detail'){
          //   return ...;
          // }else if(settings.name == '/something-else'){
          //   return ...;
          // }
          // return MaterialPageRoute(builder: (ctx) => CategoriesScreen(),);
        },
        // onUnknownRoute is reached when flutter fails to build a screen
        // with all other measures, it can load a custom error message like 404
        onUnknownRoute: (settings) {
          return MaterialPageRoute(
            builder: (ctx) => CategoriesScreen(),
          );
        });
  }
}
