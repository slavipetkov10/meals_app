import 'package:flutter/material.dart';

class Category {
  final String id;
  final String title;
  final Color color;

  // The properties can't be changed after the object is created
  const Category({
    @required this.id,
    @required this.title,
    this.color = Colors.orange,
  });
}
